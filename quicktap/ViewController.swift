//
//  ViewController.swift
//  quicktap
//
//  Created by Walter Fettich on 22/02/2017.
//  Copyright © 2017 Walter Fettich. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var buttons: [TimedButton] = []
    var timerShow:Timer?
    
    var timeUntilShow:TimeInterval = 1
    
    var hits:Int = 0
    {
        didSet
        {
            labelHits.text = "\(hits)"
        }
    }
    
    var misses:Int = 0
    {
        didSet
        {
            labelMisses.text = "\(misses)"
        }
    }
    
    @IBOutlet weak var labelMisses: UILabel!
    @IBOutlet weak var labelHits: UILabel!
    @IBOutlet weak var viewPlayfield: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        startShowingButtons()
        labelHits.text = "\(hits)"
        labelMisses.text = "\(misses)"
        
        Thread.detachNewThread
        {
            var a = self.random(from: 1, to: 20)
            a = a+30
            
        }
        
    }
    
    func startShowTimer()
    {
//        timeUntilShow = random(from: 0, to: 2, steps: 100)
        timerShow = Timer.scheduledTimer(withTimeInterval: timeUntilShow, repeats: false, block: { (Timer) in
            
            self.showButton()
//            self.startShowTimer()
        })
    }
    
    func startShowingButtons()
    {
        showButton()
//        startShowTimer()
    }
    
    func onHideButton(button:TimedButton,hit:Bool)
    {
        if hit
        {
            self.hits = self.hits + 1
        }
        else
        {
            self.misses = self.misses + 1
        }
        
        var i = self.buttons.index(of: button)
        self.buttons.remove(at: i!)
        
        self.startShowTimer()
    }
    
    func showButton ()
    {
        let w:CGFloat = CGFloat(random(from: 40, to: 100))
        let h:CGFloat = CGFloat(random(from: 40, to: 100))
        
        var button = TimedButton(type:.custom)
        button.timeToShow = random(from: 1, to: 6)
        
        let x = random(from: 0, to: UInt32(self.viewPlayfield.bounds.size.width-w))
        let y = random(from: 0, to: UInt32(self.viewPlayfield.bounds.size.height-h))
        let o = CGPoint(x:CGFloat(x),y:CGFloat(y))
        button.frame = CGRect(origin:o,size:CGSize(width:w,height:h))
        let red = CGFloat(random(from: 0, to: 255) / 255.0)
        let blue = CGFloat(random(from: 0, to: 255) / 255.0)
        let green = CGFloat(random(from: 0, to: 255) / 255.0)
        button.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1)
        button.addTarget(self, action: #selector(tappedButton), for: .touchUpInside)
        
        viewPlayfield.addSubview(button)
        
        button.onHide =
        { hit in
            
            self.onHideButton(button: button, hit: hit)
            
        }
        button.startTimer()
        
        buttons.append(button)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tappedButton(sender:TimedButton)
    {
        sender.hide(hit:true)
    }
    
    func random (from:UInt32, to:UInt32,steps:UInt32 = 1) -> Double
    {
        return Double(
            Int((arc4random_uniform(to*steps - from*steps + 1) + from*steps)
                )
            / Int(steps)
        )
    }

}

