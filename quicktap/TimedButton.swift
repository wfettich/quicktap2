//
//  TimedButton.swift
//  quicktap
//
//  Created by Walter Fettich on 28/02/2017.
//  Copyright © 2017 Walter Fettich. All rights reserved.
//

import UIKit

class TimedButton: UIButton {

    var timeToShow:TimeInterval = 5
    var timer:Timer?
    var onHide: (_ hit:Bool) -> Void = { (hit) in }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func hide(hit:Bool)
    {
        self.removeFromSuperview()
        self.timer?.invalidate()
        
        self.onHide(hit)

    }
    func startTimer()
    {
        
        setTitle("\(Int(timeToShow))", for: UIControlState.normal)
        titleLabel!.textColor = UIColor.black

        guard timeToShow > 0
        else
        {
            print ("timeToShow == 0")
            return
        }
        
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true)
        {
            (timer) in
            self.timeToShow = self.timeToShow - 1
            self.setTitle("\(Int(self.timeToShow))", for: UIControlState.normal)
            if self.timeToShow == 0
            {
                self.hide(hit:false)
            }
        }
    }
    
}
